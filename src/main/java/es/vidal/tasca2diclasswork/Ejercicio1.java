package es.vidal.tasca2diclasswork;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Ejercicio1 extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Ejercicio1.class.getResource("Exercici1.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Exercici 1 Jordi Vidal");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}