module es.vidal.tasca2diclasswork {
    requires javafx.controls;
    requires javafx.fxml;


    opens es.vidal.tasca2diclasswork to javafx.fxml;
    exports es.vidal.tasca2diclasswork;
}